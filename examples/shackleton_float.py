import rioxarray
from pathlib import Path
import pyFluid

first_cycle = 155
last_cycle = 214


basepath = Path(r'C:\Users\low\OneDrive - University of Tasmania\Research\IceFloatTracking')
matfile = Path(basepath, 'A7900904_py.mat')
xlfile = Path(basepath,'Shackleton_Float_Summary.xlsx')

ice_float = pyFluid.IceFloat(7900904, xlfile, matfile)


xr = rioxarray.open_rasterio(Path(basepath, 'BedMachineAntarctica-v3.nc'))
xr.rio.reproject('EPSG:4326')


ice_bathymetry = pyFluid.bathymetry(Path(basepath, 'BedMachineAntarctica-v3.nc'), None)

xr = rioxarray.open_rasterio(Path(basepath, 'BedMachineAntarctica-v3.nc'))
ice_draft = xr.thickness - xr.surface
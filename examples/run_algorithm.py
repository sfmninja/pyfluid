import numpy as np
import pyFluid
import os
import platform
from pathlib import Path

float_no = 7900331

# Provide algorithm arguments
fastest_speed = 0.13
lockdown_size = 1*np.pi*1500**2
bathymetry_threshold = 30
use_berg_mask = False
pressure_under_ice = 60

# Choose the first and last cycle to run algorithm
first_known = 0  # If all use 0
last_known = -1  # If all use -1

# Path to data folder ... example data is available from https://doi.org/10.6084/m9.figshare.12015126
# This example expects a directory structure as follows;
# base_path
# |_Bathymetry
#   |_bathymetryGrid.mat
# |_Floats
#   |_position_data
#       |_ WMOID_position_data.xlsx
#   |_RTmatPy
#       |_ float_WMOID.mat
#   |_output
#       |_ x.shp
# An example of this structure is provided in the data folder of this repository
# todo: create an option to read data directly from netcdf format
base_path = Path('./data/')
output_folder = Path('./data/output')
bathPath = Path(base_path, 'Bathymetry', 'BathymetryGrid.mat')        # Bathymetry
bergMaskPath = Path(base_path, 'Icebergs','bergMask.mat')
float_xls = Path(base_path, 'Floats', 'position_data', '{}_position_data.xlsx'.format(float_no))
float_mat = Path(base_path, 'Floats','RTmatPy','float{}_py.mat'.format(float_no))

# Create a pyfluid object
ice_float = pyFluid.IceFloat(float_no, float_xls, float_mat)

# Create our bathymetry object and subset to our area of interest
ice_bathymetry = pyFluid.bathymetry(bathPath, None)
[aoi_lat, aoi_lon] = ice_float.get_area_of_interest(buffer=0.5)
ice_bathymetry.subset(aoi_lat, aoi_lon)

# Calculate our slope map
ice_bathymetry.calculate_slope_map(1)

ice_bathymetry.project_bathymetry(ice_float.p1,ice_float.p2)

# Make a slope factor map
# todo: add the slope factor as a flag in the calculate_slope_map code
slope_factor = ice_bathymetry.slope.copy()
slope_factor[:] = 1
slope_factor[ice_bathymetry.slope >= 60] = 1 + (ice_bathymetry.slope[ice_bathymetry.slope >= 60] - 60)/20
slope_factor[ice_bathymetry.slope >= 80] = 2

# Define the initial float location
ice_float.define_initial_location(ice_bathymetry,
                                  slope_factor,
                                  threshold=bathymetry_threshold,
                                  use_berg_mask=use_berg_mask,
                                  min_pres=pressure_under_ice)

# Run a course predict to get starting speed
course_speeds = np.arange(fastest_speed, 0.01, -0.01)
stop_speed = ice_float.predict(course_speeds, first_known, last_known, lockdown_size, stop_on_fail=True)

# Ok now we can run fine speeds
ind = np.where(course_speeds == stop_speed)[0][0]
if ind == 0:
    print('Float travels faster than top speed please adjust')
    speed_matrix = []
else:
    speed_matrix = np.arange(course_speeds[ind-1], 0, -0.0001)
stop_speed = ice_float.predict(speed_matrix, first_known, last_known, lockdown_size, stop_on_fail=False)

# finalise positions and project back to lat, lon
ice_float.get_position()
ice_float.project_geom_to_lat_lon()

# Export as shapefiles
ice_float.export_uncertainty_polygon_to_shp(np.arange(ice_float.prediction['start_cycle'],
                                                      ice_float.prediction['end_cycle']),
                                            Path(Path().absolute(),output_folder,
                                                         'prediction_{}_poly.shp'.format(float_no)))
ice_float.export_location_to_shp(np.arange(ice_float.prediction['start_cycle'],
                                           ice_float.prediction['end_cycle']),
                                 os.path.join(Path().absolute(),output_folder, 'prediction_{}.shp'.format(float_no)))

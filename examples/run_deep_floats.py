import numpy as np
import pyFluid
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import pearsonr
from math import sin, cos, sqrt, atan2, radians
import os
import cmocean

def calculate_gaps(deep_float):

    cs = []
    css = []
    gaps = []

    bad_cycs = deep_float.data.loc[deep_float.data.POSITION_QC > 3]

    j = 1
    for i, row in bad_cycs.iterrows():
        if j < bad_cycs.shape[0]:
            gap = bad_cycs.index[j] - i
            j = j + 1
            print(gap)
            if gap == 1:
                if len(gaps) == 0:
                    gaps.append(i-1)
            else:
                if len(gaps) > 0:
                    gaps.append(i+1)
                    cs.append(gaps)
                    css.append(gaps[1] - gaps[0])
                    gaps = []
        else:
            if len(gaps) > 0:
                gaps.append(i+1)
                cs.append(gaps)
                css.append(gaps[1] - gaps[0])
                gaps = []

    return cs

def get_distance(point1, point2):
    R = 6370
    lat1 = radians(point1[0])  # insert value
    lon1 = radians(point1[1])
    lat2 = radians(point2[0])
    lon2 = radians(point2[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return distance

lockdown_size = 1.5*np.pi*1500**2
bathymetry_threshold = 60
use_berg_mask = False
pressure_under_ice = None

# Choose the first and last cycle to run algorithm
first_known = 0  # If all use 0
last_known = -1  # If all use -1
base_path = 'C:/Users/Public/Data/ice-float-path-data/deep_argo/'
output_folder = os.path.join(base_path, 'prediction/')
bathPath = os.path.join(base_path, 'bathymetry/IBCSO_v1_is_PS71_500m_tif/ibcso_v1_is_4326.tif')  # Bathymetry
slopePath = os.path.join(base_path, 'IBCSO_v1_is_PS71_500m_tif/ibcso_v1_sid_4326_distance.tif')  # Bathymetry

floats = [7900636] #5905268,7900677 5905269, 7900678, 7900637,

for float_no in floats:
    netcdf_folder = os.path.join(base_path, f'netcdf_files/{float_no}/profiles/')

    deep_float_o = pyFluid.ArgoFloat(float_no, netcdf_folder=netcdf_folder)
    deep_float = pyFluid.ArgoFloat(float_no, netcdf_folder=netcdf_folder)
    [aoi_lat, aoi_lon] = deep_float.get_area_of_interest(buffer=1)
    bathymetry = pyFluid.bathymetry(bathPath, slopePath)
    bathymetry.subset(aoi_lat, aoi_lon)

    #todo: p1 should be bathymetry coordinates
    bathymetry.project_bathymetry(p1=deep_float.p1, p2=deep_float.p2)
    deep_float.extract_depth_at_location(bathymetry)
    deep_float.apply_grounding_flag()

    if np.any(deep_float.data.POSITION_QC == 8):
        deep_float.set_time_steps()
        deep_float.set_next_known_cycle()

        deep_float.define_initial_location(bathymetry, None, threshold=bathymetry_threshold,
                                            use_berg_mask=use_berg_mask, min_pres=pressure_under_ice)

        gaps = calculate_gaps(deep_float)

        for gap in gaps:
            speeds = np.arange(0.1, 0.01, -0.01)
            success, stop_speed = deep_float.predict(speeds, start_cycle=gap[0],
                                                     end_cycle=gap[1], lockdown_size=lockdown_size,
                                                     stop_on_fail=True)

            for iSpeed in [0.001, 0.0001]:
                print(f'Predicting at {iSpeed}')

                ind = np.where(speeds == stop_speed)[0][0]
                if ind == 0:
                    print('Float travels faster than top speed please adjust')
                    speeds = []
                else:
                    speeds = np.arange(speeds[ind - 1], 0, -iSpeed)
                success, stop_speed = deep_float.predict(speeds, start_cycle=gap[0],
                                                 end_cycle=gap[1], lockdown_size=lockdown_size,
                                                 stop_on_fail=True)

            # Get linear interpolated position
            delta_lat = (deep_float.data.loc[gap[0]].LATITUDE - \
                         deep_float.data.loc[gap[1]].LATITUDE) / (deep_float.data.loc[gap[1]].JULD - deep_float.data.loc[gap[0]].JULD)
            delta_lon = (deep_float.data.loc[gap[0]].LONGITUDE - \
                         deep_float.data.loc[gap[1]].LONGITUDE) / (deep_float.data.loc[gap[1]].JULD - deep_float.data.loc[gap[0]].JULD)


            deep_float.data.loc[gap[0]+1:gap[1]-1,'LATITUDE'] = deep_float.data.loc[gap[0]].LATITUDE + \
                                            (delta_lat) * (deep_float.data.loc[gap[0]].JULD - deep_float.data.loc[gap[0]+1:gap[1]-1].JULD)
            deep_float.data.loc[gap[0] + 1:gap[1] - 1, 'LONGITUDE'] = deep_float.data.loc[gap[0]].LONGITUDE + \
                                                                       (delta_lon) * (deep_float.data.loc[gap[0]].JULD - deep_float.data.loc[
                                                                                               gap[0] + 1:gap[1] - 1].JULD)


        [aoi_lat, aoi_lon] = deep_float.get_area_of_interest(buffer=0.2)
        bathymetry.subset(aoi_lat, aoi_lon)
        ind = deep_float.data.bathymetry_depth < 100000




        fig, a = plt.subplots(1,3, figsize=[21,7], sharex=False, sharey=False)
        a[2].plot(deep_float.data.JULD, -deep_float.data.max_pressure, 'k-')
        a[2].plot(deep_float.data.JULD, -deep_float.data.bathymetry_depth, 'r-')
        a[2].set_ylim([-(deep_float.data.mission_pressure.max() + 300), 0])

        deep_float.plot_float_location(ax=a[0], show_path=True)
        a[0].set_title('Linear interpolation')
        bathymetry.plot_bathymetry_grid(ax=a[0], depth_range=[deep_float.data.bathymetry_depth.min(),deep_float.data.bathymetry_depth.loc[ind].max()], cmap=cmocean.cm.gray, showColorbar=False)

        deep_float.get_position()
        deep_float.project_geom_to_lat_lon()
        deep_float.extract_depth_at_location(bathymetry)
        a[2].plot(deep_float.data.JULD, -deep_float.data.bathymetry_depth, 'g-')
        a[2].plot(deep_float.data.JULD, -deep_float.data.mission_pressure, 'c-')
        a[2].legend(['Float recorded depth', 'Bathymetry @ linear', 'Bathymetry @ pyFluid', 'Mission Depth'])
        a[2].set_ylabel('Depth (m)')
        a[2].set_xlabel('Julian Day')

        deep_float.plot_float_location(ax=a[1], show_path=True)
        a[1].set_title('pyFluid prediction')


        fb, aa, c = bathymetry.plot_bathymetry_grid(ax=a[1], depth_range=[deep_float.data.bathymetry_depth.min(),deep_float.data.bathymetry_depth.loc[ind].max()], cmap=cmocean.cm.gray, showColorbar=False)
        cbaxes = fig.add_axes([0.14, 0.08, 0.47, 0.03])
        cb = plt.colorbar(c, cax=cbaxes, orientation="horizontal")
        cbaxes.set_xlabel('Depth (m)')
        for ax in a:
            pos1 = ax.get_position()  # get the original position
            pos2 = [pos1.x0, pos1.y0 + 0.07, pos1.width, pos1.height]
            ax.set_position(pos2)


        #deep_float.plot_uncertainty_polygon(cycles=deep_float.data.index, ax=a[2])
        #bathymetry.plot_bathymetry_grid(ax=a[2], depth_range=[deep_float.data.bathymetry_depth.min(),deep_float.data.bathymetry_depth.max()], cmap=cmocean.cm.gray, showColorbar=True)

        a[1].set_xlabel('Longitude')


        a[0].set_ylabel('Latitude')
        a[0].set_xlabel('Longitude')


        fig.savefig(os.path.join(output_folder, f'{float_no}_predicted.png'))



        deep_float.data.to_csv(os.path.join(output_folder, f'{float_no}_predicted.csv'), float_format='%11.8f')
        #deep_float.export_uncertainty_polygon_to_shp(np.arange(deep_float.prediction['start_cycle'],
        #                                                      deep_float.prediction['end_cycle']),
        #                                            os.path.join(output_folder,
        #                                                         'prediction_{float_no}_poly.shp'))
        #deep_float.export_location_to_shp(np.arange(deep_float.prediction['start_cycle'],
        #                                           deep_float.prediction['end_cycle']),
        #                                 os.path.join(output_folder, f'prediction_{float_no}.shp'))


        plt.show()
    else:
        print('No cycles to predict')



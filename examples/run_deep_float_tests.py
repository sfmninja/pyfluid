import numpy as np
import pyFluid
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import pearsonr
from math import sin, cos, sqrt, atan2, radians
import os


def calculate_gaps(good_cycs):
    cs = []
    css = []
    gaps = []

    j = 1
    for i, row in good_cycs.iterrows():
        if j < good_cycs.shape[0]:
            gap = good_cycs.index[j] - i
            j = j + 1

            if gap == 1:
                if len(gaps) == 0:
                    gaps.append(i)
            else:
                if len(gaps) > 0:
                    gaps.append(i)
                    cs.append(gaps)
                    css.append(gaps[1] - gaps[0])
                    gaps = []
        else:
            if len(gaps) > 0:
                gaps.append(i)
                cs.append(gaps)
                css.append(gaps[1] - gaps[0])
                gaps = []
    evaluation_set = pd.DataFrame(columns=['start_cyc',  'end_cyc', 'evaluation_cyc', 'gap',
                                           'known_bathy_diff', 'linear_pos', 'algo_pos', 'known_pos', 'algo_error', 'linear_error'])

    k = 0
    for ic in cs:
        for i in np.arange(3, ic[1] - ic[0], 2):
            for icc in np.arange(ic[0], ic[1] - i, i):
                if icc + i <= ic[1]:
                    evaluation_set.loc[k, 'start_cyc'] = icc
                    evaluation_set.loc[k, 'end_cyc'] = icc + i
                    evaluation_set.loc[k, 'gap'] = i
                    evaluation_set.loc[k, 'evaluation_cyc'] = icc + (i + 1) / 2
                    k = k + 1

    return evaluation_set


def get_distance(point1, point2):
    R = 6370
    lat1 = radians(point1[0])  # insert value
    lon1 = radians(point1[1])
    lat2 = radians(point2[0])
    lon2 = radians(point2[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return distance

for lockdown_sizex in [2, 3]:
    for bathymetry_threshold in np.arange(30, 110, 20):
        float_nos = [7900678]#[7900679, 7900635, 5905268, 5905269, 7900636, 7900637, 7900677, 7900678]
        for float_no in float_nos:
            # Provide algorithm arguments
            fastest_speed = 0.03
            lockdown_size = lockdown_sizex*np.pi*1500**2
            #bathymetry_threshold = 50
            use_berg_mask = False
            pressure_under_ice = None

            # Choose the first and last cycle to run algorithm
            first_known = 0  # If all use 0
            last_known = -1  # If all use -1

            base_path = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo'
            output_folder = os.path.join(base_path, 'output2/')
            bathPath = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/bathymetry/IBCSO_v1_is_PS71_500m_tif/ibcso_v1_is_4326.tif'        # Bathymetry
            slopePath = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/bathymetry/IBCSO_v1_is_PS71_500m_tif/ibcso_v1_sid_4326_distance.tif'        # Bathymetry
            #Slope calculated using - https: // au.mathworks.com / help / map / ref / gradientm.html

            netcdf_folder = f'/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/netcdf_files/{float_no}/profiles/'


            # Create a pyfluid object

            # ice_float = pyFluid.IceFloat(float_no, float_xls, float_mat)
            template_float = pyFluid.ArgoFloat(float_no, netcdf_folder=netcdf_folder)
            [aoi_lat, aoi_lon] = template_float.get_area_of_interest(buffer=1)

            bathymetry = pyFluid.bathymetry(bathPath, slopePath)
            bathymetry.subset(aoi_lat, aoi_lon)
            #todo: p1 should be bathymetry coordinates
            bathymetry.project_bathymetry(p1=template_float.p1, p2=template_float.p2)
            template_float.extract_depth_at_location(bathymetry)

            #todo: consider grounded flag here
            ind = np.logical_and.reduce([template_float.data.POSITION_QC == 1,
                                         template_float.data.max_pressure < (template_float.data.mission_pressure-50),
                                         template_float.data.mission_pressure > 2000,
                                         template_float.data.max_pressure > 2000,
                                         template_float.data.bathymetry_depth < 20000
                                        ])

            if float_no == 7900678:
                template_float.data.loc[261, 'max_depth'] = 4274
                template_float.data.loc[263, 'max_depth'] = 4145

            #template_float.data.max_depth = template_float.data.bathymetrt_depth

            eval_float = calculate_gaps(template_float.data.loc[ind])

            for i, row in eval_float.iterrows():
                float_nc = pyFluid.ArgoFloat(float_no, netcdf_folder=netcdf_folder)
                float_nc.data.loc[row.start_cyc+1:row.end_cyc-1,'POSITION_QC'] = 8
                float_nc.data.max_depth = template_float.data.max_depth
                float_nc.define_initial_location(bathymetry, None, threshold=bathymetry_threshold,
                                                 use_berg_mask=use_berg_mask, min_pres=pressure_under_ice)
                eval_float.loc[i].known_pos = [template_float.data.loc[row.evaluation_cyc].LATITUDE,
                                               template_float.data.loc[row.evaluation_cyc].LONGITUDE]

                eval_float.loc[i].known_bathy_diff = template_float.data.loc[row.evaluation_cyc].LATITUDE

                # Get linear interpolated position
                delta_lat = (template_float.data.loc[row.start_cyc].LATITUDE - \
                            template_float.data.loc[row.end_cyc].LATITUDE)/row.gap
                delta_lon = (template_float.data.loc[row.start_cyc].LONGITUDE - \
                            template_float.data.loc[row.end_cyc].LONGITUDE)/row.gap
                eval_float.loc[i].linear_pos = [template_float.data.loc[row.start_cyc].LATITUDE + \
                             (delta_lat) * (row.start_cyc - row.evaluation_cyc),
                                template_float.data.loc[row.start_cyc].LONGITUDE + \
                             (delta_lon) * (row.start_cyc - row.evaluation_cyc)]

                speeds = np.arange(0.2, 0.01, -0.01)
                success, stop_speed = float_nc.predict(speeds, row.start_cyc,
                                                       row.end_cyc, lockdown_size, stop_on_fail=True,
                                                       stop_once_found=row.evaluation_cyc)

                for iSpeed in [0.001, 0.0001]:
                    print(f'Predicting at {iSpeed}')
                    if float_nc.data.POSITION_QC.loc[row.evaluation_cyc] > 3:
                        success = 0

                    if np.logical_and(len(speeds) > 0, success == 0):
                        ind = np.where(speeds == stop_speed)[0][0]
                        if ind == 0:
                            print('Float travels faster than top speed please adjust')
                            speeds = []
                        else:
                            speeds = np.arange(speeds[ind-1], 0, -iSpeed)
                        success, stop_speed = float_nc.predict(speeds,
                                                      row.start_cyc,
                                                      row.end_cyc, lockdown_size, stop_on_fail=False,
                                                      stop_once_found=row.evaluation_cyc)
                    else:
                        break

                float_nc.get_position()
                float_nc.project_geom_to_lat_lon()
                #float_nc.plot_float_location()
                #plt.savefig(f'{output_folder}figures/{float_no}_{row.start_cyc}_{row.end_cyc}.png')

                eval_float.loc[i].algo_pos = [float_nc.data.loc[row.evaluation_cyc].LATITUDE, float_nc.data.loc[row.evaluation_cyc].LONGITUDE]
                if ~np.isinf(eval_float.loc[i].algo_pos[0]):
                    eval_float.loc[i].algo_error = get_distance(eval_float.loc[i].algo_pos,
                                                            eval_float.loc[i].known_pos)
                    eval_float.loc[i].algo_speed = stop_speed
                eval_float.loc[i].linear_error = get_distance(eval_float.loc[i].linear_pos,
                                                            eval_float.loc[i].known_pos)

            eval_float.to_csv(f'{output_folder}{float_no}_{bathymetry_threshold}_{lockdown_sizex}.csv')
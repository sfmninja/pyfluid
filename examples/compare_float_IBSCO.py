import numpy as np
import pyFluid
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import pearsonr

float_nos = [7900679, 7900635, 5905268, 5905269, 7900636, 7900637, 7900677, 7900678]

# Provide algorithm arguments
fastest_speed = 0.03
lockdown_size = 1*np.pi*1500**2
bathymetry_threshold = 30
use_berg_mask = False
pressure_under_ice = 60

# Choose the first and last cycle to run algorithm
first_known = 0  # If all use 0
last_known = -1  # If all use -1

# Path to data folder ... example data is available from https://doi.org/10.6084/m9.figshare.12015126
# This example expects a directory structure as follows;
# base_path
# |_Bathymetry
#   |_bathymetryGrid.mat
# |_Floats
#   |_position_data
#       |_ WMOID_position_data.xlsx
#   |_RTmatPy
#       |_ float_WMOID.mat
#   |_output
#       |_ x.shp
# An example of this structure is provided in the data folder of this repository

base_path = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo'
output_folder = './data/output'
bathPath = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/bathymetry/IBCSO_v1_is_PS71_500m_tif/ibcso_v1_is_4326.tif'        # Bathymetry
slopePath = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/bathymetry/IBCSO_v1_is_PS71_500m_tif/ibcso_v1_sid_4326_distance.tif'        # Bathymetry
#Slope calculated using - https: // au.mathworks.com / help / map / ref / gradientm.html

results = pd.DataFrame(columns=['float_no', 'LATITUDE', 'LONGITUDE', 'max_depth', 'bathymetry_depth', 'bathymetry_slope', 'mission_pressure'])
for float_no in float_nos:
    print(f'Running float {float_no}')
    netcdf_folder = f'/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/netcdf_files/{float_no}/profiles/'

    # Create a pyfluid object

    #ice_float = pyFluid.IceFloat(float_no, float_xls, float_mat)
    ice_float_nc = pyFluid.IceFloat(float_no, netcdf_folder=netcdf_folder)
    [aoi_lat, aoi_lon] = ice_float_nc.get_area_of_interest(buffer=1)


    ice_bathymetry = pyFluid.iceBathymetry(bathPath, slopePath)
    ice_bathymetry.subset(aoi_lat, aoi_lon)
    ice_bathymetry.project_bathymetry(p1=ice_float_nc.p1, p2=ice_float_nc.p2)
    ice_float_nc.extract_depth_at_location(ice_bathymetry)

    #ice_float_nc.define_initial_location(ice_bathymetry, 1, threshold=bathymetry_threshold, use_berg_mask=use_berg_mask, min_pres=pressure_under_ice)
    ind = np.logical_and.reduce([ice_float_nc.data.POSITION_QC == 1,
                                 ice_float_nc.data.max_pressure < (ice_float_nc.data.mission_pressure-50),
                                 ice_float_nc.data.mission_pressure > 2000,
                                 ice_float_nc.data.max_pressure > 2000,
                                 ice_float_nc.data.bathymetry_depth < 20000
                                 ])
    if np.any(ind):
        results = results.append(ice_float_nc.data.loc[ind][['LATITUDE', 'LONGITUDE', 'max_depth', 'bathymetry_depth', 'bathymetry_slope', 'mission_pressure']])
        results.loc[np.isnan(results.float_no.values.astype(float)), 'float_no'] = float_no

        good_cycs = ice_float_nc.data.loc[ind]

        breaks = []
        cs = []
        css = []
        pi = -3
        for i, row in good_cycs.iterrows():
            gap = i - pi
            if gap == 1:
                cs.append(i)
            else:
                cs = []
                css.append(cs)
                breaks.append(i)
            pi = i
        cs_s = []
        for i in css:
            cs_s.append(len(i)+1)


        f, a = plt.subplots(2, 2)
        a[0, 0].set_title(float_no)
        a[0, 0].plot([3000, 5000], [3000, 5000], 'r-')
        a[0, 0].scatter(good_cycs.max_depth, good_cycs.bathymetry_depth)
        a[0, 0].axis('equal')
        a[0, 0].set_xlabel('float depth')
        a[0, 0].set_ylabel('bathymetry depth')
        a[1, 0].hist(good_cycs.max_depth - good_cycs.bathymetry_depth)
        a[1, 0].set_xlabel('float depth - bathymetry depth')
        a[1, 0].set_ylabel('Number of cycles')

        a[1, 1].scatter(good_cycs.LONGITUDE, good_cycs.LATITUDE, np.abs(good_cycs.max_depth - good_cycs.bathymetry_depth), alpha=0.5)
        a[1, 1].set_xlabel('Longitude')
        a[1, 1].set_ylabel('Latitude')
        a[0, 1].scatter(good_cycs.bathymetry_slope,
                        np.abs(good_cycs.max_depth - good_cycs.bathymetry_depth))
        a[0, 1].set_xlabel('Distance to measurement')
        a[0, 1].set_ylabel('float depth - bathymetry depth')
        plt.show()

        print(f'Float {float_no}')
        print(f'Total of {ice_float_nc.data.shape[0]} locations of which {good_cycs.shape[0]} known')
        print(f'Maxium consectutive known locations {np.max(cs_s)}')
        print(f'Consectutive known location groups {len(cs_s)}')
        rmse = np.sqrt(np.sum((good_cycs.max_depth - good_cycs.bathymetry_depth)**2)/good_cycs.shape[0])
        print(f'RMSE = {rmse:.2f}')
        bias = np.sum((good_cycs.max_depth - good_cycs.bathymetry_depth))/good_cycs.shape[0]
        print(f'bias = {bias:.2f}')
        r, _ = pearsonr(good_cycs.bathymetry_slope, np.abs(good_cycs.max_depth - good_cycs.bathymetry_depth))
        print(f'Pearsons correlation = {r:.3f}')

f,a = plt.subplots(2,2)
a[0,0].plot([3000, 5000], [3000, 5000], 'r-')
a[0,0].scatter(results.max_depth, results.bathymetry_depth)
a[0,0].axis('equal')
a[0,0].set_xlabel('float depth')
a[0,0].set_ylabel('bathymetry depth')
a[1,0].hist(results.max_depth -results.bathymetry_depth)
a[1,0].set_xlabel('float depth - bathymetry depth')

a[1,1].scatter(results.LONGITUDE, results.LATITUDE, np.abs(results.max_depth -results.bathymetry_depth), alpha=0.5)
a[1,1].set_xlabel('Longitude')
a[1,1].set_ylabel('Latitude')
a[0,1].scatter(results.bathymetry_slope, np.abs(results.max_depth -results.bathymetry_depth))
a[1,1].set_xlabel('Distance to measurement')
a[1,1].set_ylabel('float depth - bathymetry depth')
plt.show()

ind = np.abs(results.max_depth - results.bathymetry_depth) < 200

rmse = np.sqrt(np.sum((results.loc[ind].max_depth - results.loc[ind].bathymetry_depth)**2)/results.shape[0])
print(f'RMSE = {rmse:.2f}')
bias = np.sum((results.loc[ind].max_depth - results.loc[ind].bathymetry_depth))/results.loc[ind].shape[0]
print(f'bias = {bias:.2f}')
r, _ = pearsonr(results.loc[ind].bathymetry_slope, np.abs(results.loc[ind].max_depth - results.loc[ind].bathymetry_depth))
print(f'Pearsons correlation = {r:.3f}')
import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
path = '/mnt/spinny-store/Luke/Data/ice-float-path-data/deep_argo/output2/'

files = os.listdir(path)
data = pd.DataFrame()


for file in files:
    if file[-5:] == '1.csv':
        d = pd.read_csv(f'{path}{file}')
        d['lockdown_size'] = 1
        d['diff_threshold'] = np.int(file.split('_')[1].split('.')[0])
        data = data.append(d)


for i in [10, 30, 50, 70, 100]:
    ind = data['diff_threshold'] == i
    data.loc[ind].plot.scatter('algo_error', 'linear_error')
    plt.plot([0, 400], [0, 400], 'r-')
    plt.show()

    print('')
    print(f'Lockdown size {i}')
    for ig in data.gap.unique():
        print(f'    Gap {ig} n = {np.count_nonzero( data.loc[ind].gap == ig)}')
        print(f'    Gap {ig} Algo: { data.loc[ind].loc[ data.loc[ind].gap == ig].algo_error.mean()}')
        print(f'    Gap {ig} linear: { data.loc[ind].loc[ data.loc[ind].gap == ig].linear_error.mean()}')
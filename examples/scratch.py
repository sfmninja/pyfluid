from argopy import DataFetcher as ArgoDataFetcher
import argopy

argo_loader = ArgoDataFetcher()

ds = argo_loader.float(6902746)

with argopy.set_options(src='localftp', local_ftp=''):
    ds = ArgoDataFetcher().float(1900857).to_xarray()
    print(ds)

ds.to_xarray()
from shapely import geometry
from descartes import PolygonPatch
from matplotlib.collections import PatchCollection

def plot_multipolygon(mp, ax, c):
    patches = []

    if isinstance(mp, geometry.multipolygon.MultiPolygon):
        for p in mp:
            #patches.append(PolygonPatch(p, fc='None', ec=c, alpha=1., zorder=1))
            x, y = p.exterior.xy
            ax.plot(y, x, c=c)
    else:
        x, y = mp.exterior.xy
        ax.plot(y, x, c=c)

    #ax.add_collection(PatchCollection(patches, match_original=True), autolim=True)
import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np
import pyproj
import rasterio
import rioxarray

class bathymetry:
    """ iceBathymetry class to handle importing, projecting and querying imported bathymetry.
        """

    def __init__(self, bath_file_path, iceberg_file_path=None, slope_map=None):
        '''

        :param bath_file_path: file path to the bathymetry grid in mat format
        :param iceberg_file_path: path to the iceberg mask in mat format an assumption is made that the
                                  iceberg mask is in the same grid projection as the bathymetry
        '''
        if bath_file_path.suffix == '.mat':
            bathymetry_grid = sio.loadmat(bath_file_path)
            self.depth = bathymetry_grid['A']
            self.latitude = bathymetry_grid['latg']
            self.longitude = bathymetry_grid['long']
            self.slope = None

            if iceberg_file_path is None:
                self.ice_berg_grid = None
            else:
                if iceberg_file_path[-3:] == 'npz':
                    self.ice_berg_grid = np.load(iceberg_file_path)['mask']
                else:
                    self.ice_berg_grid = sio.loadmat(iceberg_file_path)['mask']

            if slope_map is None:
                self.calculate_slope_map(1)
            else:
                slope_grid = sio.loadmat(slope_map)
                self.slope = slope_grid['slope']
        elif bath_file_path.suffix == '.nc':
            xr = rioxarray.open_rasterio(bath_file_path)
            xr.rio.reproject(4326)
            self.depth = xr.bed

        else:
            dataset = rasterio.open(bath_file_path)
            self.depth = dataset.read(1)

            nx, ny = self.depth.shape
            x = np.linspace(0, nx, nx)
            y = np.linspace(0, ny, ny)
            xv, yv = np.meshgrid(y, x)

            self.longitude, self.latitude = rasterio.transform.xy(dataset.transform, yv, xv)
            self.longitude = np.array(self.longitude)
            self.latitude = np.array(self.latitude)
            self.slope = None
            self.ice_berg_grid = None

            if slope_map is None:
                self.calculate_slope_map(1)
            else:
                dataset = rasterio.open(slope_map)
                self.slope = dataset.read(1)

    def project_bathymetry(self, p1, p2):
        ''' project_bathymetry: project the point locations of the bathymetry into a new cooridinate system

        :param p1: from projection
        :param p2: to projection
        :return:
        '''
        transformer = pyproj.Transformer.from_crs(p1, p2, always_xy=True)
        self.Eastings, self.Northings = transformer.transform(self.longitude,
                                                              self.latitude)

    def subset(self, latitude_range, longitude_range):
        '''

        :param latitude_range: minimum and maximum latitude to include in bathymetry
        :param longitude_range: minimum and maximum longitude to include in bathymetry
        :return:
        '''

        # Find the indices we want to keep
        ind_lat = np.logical_and(np.greater(self.latitude, latitude_range[0]),
                                 np.less(self.latitude, latitude_range[1]))
        ind_lon = np.logical_and(np.greater(self.longitude, longitude_range[0]),
                                 np.less(self.longitude, longitude_range[1]))
        ind = np.logical_and.reduce([ind_lat, ind_lon, self.depth < -10])

        # Now select area from all our grids
        self.depth = self.depth[:, ind.any(0)]
        self.latitude = self.latitude[:, ind.any(0)]
        self.longitude = self.longitude[:, ind.any(0)]
        self.slope = self.slope[:, ind.any(0)]
        if self.ice_berg_grid is not None:
            self.ice_berg_grid = self.ice_berg_grid[:, ind.any(0)]
        ind = ind[:, ind.any(0)]


        self.depth = self.depth[ind.any(1), :]
        self.latitude = self.latitude[ind.any(1), :]
        self.longitude = self.longitude[ind.any(1),:]
        self.slope = self.slope[ind.any(1), :]
        if self.ice_berg_grid is not None:
            self.ice_berg_grid = self.ice_berg_grid[ind.any(1),:]

        # Add a 5000 m border such that all contours close
        self.depth[:,-1] = 0
        self.depth[:, 0] = 0
        self.depth[-1, :] = 0
        self.depth[0, :] = 0

    def plot_bathymetry_grid(self, ax=None, fig=None, depth_range = [-2000, 0], type='depth', projected=False, cmap='viridis', showColorbar=True):
        if ax is None:
            fig, ax = plt.subplots()

        if type == 'depth':
            p_grid = -self.depth
            dr = depth_range
        elif type == 'slope':
            p_grid = self.slope
            dr = [0, 100]
        elif type == 'ice_berg':
            p_grid = self.ice_berg_grid
            dr = [0, 2]

        if projected:
            c = ax.pcolor(self.Eastings, self.Northings, p_grid, cmap=cmap, vmin=dr[0], vmax=dr[1])
        else:
            c = ax.pcolor(self.longitude, self.latitude, p_grid, cmap=cmap, vmin=dr[0], vmax=dr[1])

        if showColorbar:
            cb1 = plt.colorbar(c, ax=ax)
            cb1.set_label('Depth (m)')

        return fig, ax, c

    def calculate_slope_map(self, dx):
        Zbc = self.assignBCs()
        # Compute finite differences
        Sx = (Zbc[1:-1, 2:] - Zbc[1:-1, :-2]) / (2 * dx)
        Sy = (Zbc[2:, 1:-1] - Zbc[:-2, 1:-1]) / (2 * dx)
        self.slope = np.sqrt(Sx ** 2 + Sy ** 2)


    def assignBCs(self):
        # Pads the boundaries of a grid
        # Boundary condition pads the boundaries with equivalent values
        # to the data margins, e.g. x[-1,1] = x[1,1]
        # This creates a grid 2 rows and 2 columns larger than the input

        ny, nx = self.depth.shape  # Size of array
        Zbc = np.zeros((ny + 2, nx + 2))  # Create boundary condition array
        Zbc[1:-1, 1:-1] = self.depth  # Insert old grid in center

        # Assign boundary conditions - sides
        Zbc[0, 1:-1] = self.depth[0, :]
        Zbc[-1, 1:-1] = self.depth[-1, :]
        Zbc[1:-1, 0] = self.depth[:, 0]
        Zbc[1:-1, -1] = self.depth[:, -1]

        # Assign boundary conditions - corners
        Zbc[0, 0] = self.depth[0, 0]
        Zbc[0, -1] = self.depth[0, -1]
        Zbc[-1, 0] = self.depth[-1, 0]
        Zbc[-1, -1] = self.depth[-1, 0]

        return Zbc
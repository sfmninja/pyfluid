import pandas as pd
import scipy.io as sio
from scipy.interpolate import LinearNDInterpolator
import matplotlib.pyplot as plt
from skimage import measure
from shapely import geometry
import numpy as np
from functools import partial
import pyproj
from shapely.ops import transform
from geopandas import GeoDataFrame
import pyFluid
import xarray as xr
import glob
import os
import datetime

columns = ['cycle_number', 'POSITION_QC', 'LATITUDE', 'LONGITUDE',
           'JULD', 'time_to_next', 'time_to_prev', 'next_ind', 'prev_ind',
           'bathymetry_locations', 'max_pressure', 'min_pressure', 'speed', 'apply_iceberg', 'ice_detected',
           'next_known_gap']
dtypes = [np.int16, np.int8, np.float16, np.float16, np.float64, np.float64, np.float64, np.int8, np.int8,
          np.float16, np.float16, np.float32, np.float32, np.bool, np.int8, np.int8]

class IceFloat(object):

    """ iceFloat class to handle importing, projecting, predicting and querying imported ice float.
        """

    def __init__(self, wmoid, float_xls=None, float_mat=None, netcdf_folder=None, qc_mode='D'):
        """ Initialisation of a ice float floats can either be read from a folder of netcdf files or a combination of xls
            and mat files
            :param wmoid: wmoid of the float being used
            :param float_xls: xlsx file containing float information
            :param float_mat: mat file containing float information
        """
        if np.logical_and.reduce([float_xls is None, float_mat is None, netcdf_folder is None]):
            raise Exception("Please specify float_mat and float_xls or a folder of netcdf file ")

        self.wmoid = wmoid
        self.data = pd.DataFrame({k: pd.Series(dtype=t) for k, t in zip(columns, dtypes)})
        self.p1 = pyproj.CRS.from_epsg("4326")

        if netcdf_folder is None:
            if np.logical_or(float_xls is None, float_mat is None):
                raise Exception("Both float_xls and float_mat are required to load a float")

            self.load_float_from_csiro_format(float_xls, float_mat)
        else:
            self.load_float_from_netcdf(netcdf_folder, qc_mode)

        self.data.loc[:, 'speed'] = -9999.
        self.data['apply_iceberg'] = False
        self.drop_missing()
        self.calculate_depth()
        self.set_time_steps()
        self.set_next_known_cycle()
        self.project_float_locations()

    def load_float_from_csiro_format(self, float_xls, float_mat):
        # Get our xls data
        xls_data = pd.read_excel(float_xls, sheet_name=0)
        xls_data = xls_data.set_index('cycle_number', drop=True)

        # Firstly lets create the DataFrame with cycle_number as the index
        cycle_numbers = np.zeros(xls_data.shape[0], dtype=int)
        for iCycleNumber, cycleNumber in enumerate(xls_data.index.values):
            if isinstance(cycleNumber, np.int64):
                cycle_numbers[iCycleNumber] = cycleNumber
            else:
                cycle_numbers[iCycleNumber] = cycle_numbers[iCycleNumber-1]+1
        self.data[columns[0]] = cycle_numbers.astype(np.int)

        self.data = self.data.set_index('cycle_number', drop=True)

        # Set columns that we can directly transfer
        self.set_ambiguous_columns('POSITION_QC', xls_data, ['position_qc_PF', '  Position qc PF'], data_type=int)
        self.set_ambiguous_columns('ice_detected', xls_data, ['ice_detected', ' ice_detected_flag'], data_type=int)
        self.data.loc[np.isnan(self.data.POSITION_QC), 'POSITION_QC'] = 8
        self.set_ambiguous_columns('LATITUDE', xls_data, ['latitude', ' latitude'], data_type=np.float16)
        self.set_ambiguous_columns('LONGITUDE', xls_data, ['longitude', ' longitude'], data_type=np.float16)

        # Now get our mat data
        mat_data = sio.loadmat(float_mat)
        A = mat_data['cycleNumber'].transpose()
        ind = np.nonzero(cycle_numbers[:, None] == A)[1]

        self.data.loc[cycle_numbers[ind],'JULD'] = mat_data['jDay'][ind]
        self.data.loc[cycle_numbers[ind], 'max_pressure'] = mat_data['max_depth'][ind]
        self.data.loc[cycle_numbers[ind], 'min_pressure'] = mat_data['min_depth'][ind]
        self.data['max_pressure'] = self.data['max_pressure'].astype(np.float16)
        self.data['min_pressure'] = self.data['min_pressure'].astype(np.float16)


    def load_float_from_netcdf(self, netcdf_folder, qc_mode):
        files = glob.glob(os.path.join(netcdf_folder, f'*{qc_mode}.nc'),
                          recursive=True)
        self.data = self.data.set_index('cycle_number')

        if len(files) == 0:
            raise ValueError('No netcdf files found in directory')

        meta = xr.open_mfdataset(os.path.join(netcdf_folder, f'{self.wmoid}_meta.nc'))
        ind_profile_pressure = np.where(meta.CONFIG_PARAMETER_NAME.values == b'CONFIG_ProfilePressure_dbar                                                                                                     ')[0][0]

        for f in files:
            ds_disk = xr.open_mfdataset(f)

            cyc_num = ds_disk.CYCLE_NUMBER.values[0]

            qc = ds_disk.PRES_ADJUSTED_QC.values[0, :] <= 4
            self.data.loc[cyc_num, 'max_pressure'] = np.nanmax(ds_disk.PRES_ADJUSTED.values[0, :])
            self.data.loc[cyc_num, 'min_pressure'] = np.min(ds_disk.PRES.values[0, qc])
            self.data.loc[cyc_num, 'mission_pressure'] = meta.CONFIG_PARAMETER_VALUE.values[ds_disk.CONFIG_MISSION_NUMBER.values[0].astype(int)-1,
                                                                                            ind_profile_pressure]
            self.data.loc[cyc_num, 'POSITION_QC'] = np.int8(ds_disk.POSITION_QC.values[0])
            self.data.loc[cyc_num, 'LATITUDE'] = ds_disk.LATITUDE.values[0]
            self.data.loc[cyc_num, 'LONGITUDE'] = ds_disk.LONGITUDE.values[0]
            self.data.loc[cyc_num, 'JULD'] = (datetime.datetime.fromtimestamp((ds_disk.JULD.values[0].astype('int64') / 1e9).astype('int32')) \
                                             - datetime.datetime.fromisoformat('1965-01-01')).total_seconds()/60/60/24 + 2438761.04166667
        self.data = self.data.sort_index()

    def clean_float_cycles(self, min_max_depth=100):
        ind = self.data.max_depth < min_max_depth
        self.data.loc[ind, 'JULD'] = np.nan

    def set_time_steps(self):
        # Set the next and previous time stamps
        jDay = self.data.JULD.values
        ind_good_value = self.data.index[np.where(~np.isnan(jDay))[0].astype(int)].values

        self.data['next_ind'] = -1
        self.data.next_ind = self.data.next_ind.astype(int)

        self.data['prev_ind'] = -1
        self.data.prev_ind = self.data.prev_ind.astype(int)


        self.data.loc[ind_good_value[:-1], 'next_ind'] = ind_good_value[1:].astype(int)
        self.data.loc[ind_good_value[1:], 'prev_ind'] = ind_good_value[:-1].astype(int)

        next_time = self.data.reindex(self.data.next_ind[ind_good_value[:-1]]).JULD.values
        this_time = self.data.loc[ind_good_value[:-1], 'JULD'].values
        self.data.loc[ind_good_value[:-1], 'time_to_next'] = (next_time - this_time) * 24 * 60 * 60

        prev_time = self.data.reindex(self.data.prev_ind[ind_good_value[1:]]).JULD.values
        this_time = self.data.loc[ind_good_value[1:], 'JULD'].values
        self.data.loc[ind_good_value[1:], 'time_to_prev'] = (this_time - prev_time) * 24 * 60 * 60

    def set_next_known_cycle(self):
        # Set the next and previous time stamps
        prev_known = self.data.index.min()
        for i, row in self.data.iterrows():
            if row['POSITION_QC'] == 1 or row['POSITION_QC'] == 2 or \
                    row['POSITION_QC'] == 3:
                prev_known = i
            self.data.loc[i, 'prev_known_gap'] = abs(prev_known - i)
            self.data.loc[i, 'prev_known_ind'] = prev_known

        self.data['days_to_prev_known'] = self.data['JULD']
        for i, row in self.data.iterrows():
            ind = row.prev_known_ind == self.data.index
            if any(ind):
                self.data.loc[i, 'days_to_prev_known'] = np.abs(row.JULD - self.data.loc[ind, 'JULD'].values)

        next_known = -1
        for i in np.flipud(self.data.index):
            self.data.loc[i, 'next_known_gap'] = abs(next_known - i)
            self.data.loc[i, 'next_known_ind'] = next_known
            row = self.data.loc[i]
            if row['POSITION_QC'] == 1 or row['POSITION_QC'] == 2 or \
                    row['POSITION_QC'] == 3:
                next_known = i


        self.data['days_to_next_known'] = self.data['JULD']
        for i, row in self.data.iterrows():
            ind = row.next_known_ind == self.data.index
            if any(ind):
                self.data.loc[i, 'days_to_next_known'] = np.abs(
                    row.JULD - self.data.loc[row.next_known_ind == self.data.index, 'JULD'].values)

        ind = np.isnan(self.data.max_pressure)
        self.data.loc[ind, 'next_known_gap'] = np.nan
        self.data.loc[ind, 'prev_known_gap'] = np.nan

    def set_ambiguous_columns(self, to_column_name, from_data_frame, from_column_names, data_type=np.float32):


        for c in from_column_names:
            if c in from_data_frame.columns:
                if to_column_name == 'POSITION_QC':
                    from_data_frame.loc[np.isnan(from_data_frame[c].values),c] = 9
                elif to_column_name == 'ice_detected':
                    from_data_frame.loc[np.isnan(from_data_frame[c].values), c] = 0
                self.data[to_column_name] = from_data_frame[c].astype(data_type)

    def get_position_type_string(self, cycle_no):
        pos_names = ['', 'GPS', 'IRIDIUM', 'ALGORITHM', '', '', '', 'UNKNOWN', 'MISSING CYCLE']
        ind = self.data.cycle_number == 9
        numeric_value = self.data.loc[ind].POSITION_QC.values[0]
        if np.isnan(numeric_value):
            return 'Position type unknown'
        else:
            return pos_names[numeric_value.astype('int')]

    def extract_depth_at_location(self, ice_bathymetry):
        # Create a regular grid interpolator
        rginterp = LinearNDInterpolator(
            np.vstack((ice_bathymetry.longitude.flatten(), ice_bathymetry.latitude.flatten())).transpose(),
            ice_bathymetry.depth.flatten(), fill_value=np.nan)
        self.data['bathymetry_depth'] = -rginterp(self.data[['LONGITUDE', 'LATITUDE']])

        if ice_bathymetry.slope is not None:
            rgsinterp = LinearNDInterpolator(
                np.vstack((ice_bathymetry.longitude.flatten(), ice_bathymetry.latitude.flatten())).transpose(),
                ice_bathymetry.slope.flatten())
            self.data['bathymetry_slope'] = rgsinterp(self.data[['LONGITUDE', 'LATITUDE']])

    def plot_float_location(self, position_type=range(1, 4), ax=None, plot_cycle_number=False):
        type_colours = ['w', 'r', 'b', 'g', 'w', 'w', 'r', 'g', 'c']

        if ax is None:
            fig, ax = plt.subplots()

        for pos_type in position_type:
            ind = self.data.POSITION_QC == pos_type
            if any(ind):
                ax.plot(self.data.longitude.loc[ind], self.data.latitude.loc[ind], '.', color=type_colours[pos_type])
                if plot_cycle_number:
                    for i_row, row in self.data.loc[ind, :].iterrows():
                        ax.text(row['longitude'], row['latitude'],
                                '{}'.format(i_row), fontsize=15)
        return ax

    def project_float_locations(self, p2=None, p1=None):
        if p1 is None:
            self.p1 = pyproj.CRS.from_epsg("4326")
        else:
            self.p1 = p1

        if p2 is None:
            self.p2 = pyproj.CRS.from_user_input(dict(proj='tmerc',
                                                      lat_0=np.nanmean(self.data.LATITUDE),
                                                      lon_0=np.nanmean(self.data.LONGITUDE),
                                                      ellps='WGS84'))
            #pyproj.Proj(proj='tmerc', lat_0=self.data.LATITUDE.mean(), lon_0=self.data.LONGITUDE.mean(),
            #                      ellps='WGS84')
        else:
            self.p2 = p2


        transformer = pyproj.Transformer.from_crs(self.p1, self.p2, always_xy=True)
        self.data['Eastings'], self.data['Northings'] = transformer.transform(self.data.LONGITUDE.values,
                                                                              self.data.LATITUDE.values)


    def apply_ice_berg(self, min_pres=200):
        self.data['apply_iceberg'] = np.logical_and(self.data.min_pressure > min_pres, self.data.ice_detected == 1)

    def contours_to_polygons(self, contours, ice_bathymetry):
        polys = []
        for n, contour in enumerate(contours):
            x_grid = np.int32(contour[:, 0])
            y_grid = np.int32(contour[:, 1])

            xa = ice_bathymetry.Eastings[x_grid, y_grid]
            ya = ice_bathymetry.Northings[x_grid, y_grid]
            if not (all(np.isnan(xa))):
                if any(np.isnan(xa)):
                    ind = ~(np.isnan(xa))
                    xa = xa[ind]
                    ya = ya[ind]
                    contour = contour[ind, :]

                if contour.shape[0] <= 3:
                    # point = geometry.Point(contour[1,:])
                    point = geometry.Point([xa[1], ya[1]])
                    polys.append(point.buffer(0.0).simplify(20))
                else:
                    polys.append(geometry.Polygon(np.vstack((xa, ya)).transpose()).buffer(0.0).simplify(20))
        m = geometry.MultiPolygon(polys)
        return m

    def define_initial_location(self, ice_bathymetry, slope_factor, threshold=50, GPS_buffer=50, Irridium_buffer=750,
                                simple_factor=200, use_berg_mask=False, min_pres=200):
        geometry_sets = []
        self.apply_ice_berg(min_pres=min_pres)

        for iRow, row in self.data.iterrows():
            if row.POSITION_QC == 8:
                diff_map = np.divide(np.abs(ice_bathymetry.depth + row.max_depth), slope_factor)

                contours = measure.find_contours(diff_map < threshold, 0.5)
                m = self.contours_to_polygons(contours, ice_bathymetry)

                if use_berg_mask:
                    if row.apply_iceberg:
                        if row.apply_iceberg:
                            contours_berg = measure.find_contours(ice_bathymetry.ice_berg_grid, 0.5)
                            cm = self.contours_to_polygons(contours_berg, ice_bathymetry)
                            cm = cm.buffer(1)
                            m = geometry.MultiPolygon(m.buffer(1).intersection(cm))

            elif row.POSITION_QC == 1:
                p = geometry.Point(row.Eastings, row.Northings)
                m = p.buffer(GPS_buffer)
            elif row.POSITION_QC == 2:
                p = geometry.Point(row.Eastings, row.Northings)
                m = p.buffer(Irridium_buffer)

            geometry_sets.append(m)

        self.data = GeoDataFrame(self.data, crs=self.p2, geometry=geometry_sets)
        # self.data = self.data.set_index('cycle_number', drop=True)

    def get_known_geoms(self):
        known_geoms = self.data.geometry.copy()
        known_geoms[self.data.POSITION_QC > 3] = None
        return known_geoms

    def distance_restrict(self, row, old_geom, speed, compare_row, compare_geom):
        distance = np.abs(row.JULD - compare_row.JULD) * 24 * 60 * 60 * speed

        return compare_geom.buffer(distance).intersection(old_geom.buffer(0.0))

        # def interpolate_jDay(self):
        #    for iRow, row in ice_float.data.iterrows():
        #        if np.logical_and(~np.isnan(row.max_depth), np.isnan(row.jDay)):
        #            print('me')

    def calculate_depth(self):
        DEG2RAD = np.pi / 180;
        c1 = +9.72659;
        c2 = -2.2512E-5;
        c3 = +2.279E-10;
        c4 = -1.82E-15;
        gam_dash = 2.184e-6;

        LAT = np.abs(self.data.LATITUDE);
        X = np.sin(LAT * DEG2RAD) ** 2  # convert to radians
        X = X ** 2
        P = self.data.max_pressure
        bot_line = 9.780318 * (1.0 + (5.2788E-3 + 2.36E-5 * X) * X) + gam_dash * 0.5 * P
        top_line = (((c4 * P + c3) * P + c2) * P + c1) * P
        self.data['max_depth'] = top_line / bot_line

    def predict(self, speed_matrix, start_cycle, end_cycle, lockdown_size, stop_on_fail=False):
        speed = -1
        d = self.data.loc[self.data.POSITION_QC <= 2]
        if start_cycle < d.index.min():
            start_cycle = d.index.min()

        if end_cycle == -1:
            end_cycle = d.index.max()

        elif end_cycle > d.index.max():
            end_cycle = d.index.max()
        print('Running float from cycle {} to cycle {}'.format(start_cycle, end_cycle))

        self.prediction = {}
        self.prediction['start_cycle'] = start_cycle
        self.prediction['end_cycle'] = end_cycle
        self.prediction['lockdown_size'] = end_cycle
        self.prediction['speed_matrix'] = speed_matrix

        for speed in speed_matrix:
            print('Running speed reduction {:1.4f} m/s'.format(speed))
            self.set_next_known_cycle()
            old_geom = self.data.geometry.copy()
            curr_geom = self.data.geometry.copy()

            gaps = np.sort(
                np.unique(np.hstack([self.data.loc[start_cycle:end_cycle].next_known_gap.unique(),
                                     self.data.loc[start_cycle:end_cycle].prev_known_gap.unique()])))
            gaps = gaps[~np.isnan(gaps)]
            gaps = gaps[gaps > 0]

            for iGap in gaps:
                for iRow, row in self.data.loc[start_cycle:end_cycle].iterrows():
                    if np.logical_and(row.prev_known_gap == iGap, row.POSITION_QC > 3):
                        compare_row = self.data.loc[np.int(row.prev_ind), :]
                        compare_geom = curr_geom[np.int(row.prev_ind)]
                        n_geom = self.distance_restrict(row, curr_geom[iRow], speed, compare_row, compare_geom)

                        if np.logical_and(n_geom is not None, n_geom.area > 1):
                            curr_geom.loc[iRow] = n_geom
                            if n_geom.area < lockdown_size:
                                print('   Location found for cycle {}'.format(iRow))
                                self.data.loc[iRow, 'next_ind'] = iRow
                                self.data.loc[iRow, 'prev_ind'] = iRow
                                self.data.loc[iRow, 'POSITION_QC'] = 3
                                self.data.loc[iRow, 'speed'] = speed
                        else:
                            print('path unable to resolve on cycle {}'.format(row.prev_ind))
                            if stop_on_fail:
                                self.data.geometry = old_geom.copy()
                                return speed
                            self.data.loc[iRow, 'next_ind'] = iRow
                            self.data.loc[iRow, 'prev_ind'] = iRow
                            self.data.loc[iRow, 'POSITION_QC'] = 3
                            self.data.loc[iRow, 'speed'] = speed

                    if np.logical_and(row.next_known_gap == iGap, row.POSITION_QC > 3):
                        compare_row = self.data.loc[np.int(row.next_ind), :]
                        compare_geom = curr_geom[np.int(row.next_ind)]
                        n_geom = self.distance_restrict(row, curr_geom[iRow], speed, compare_row, compare_geom)

                        if np.logical_and(n_geom is not None, n_geom.area > 1):
                            curr_geom.loc[iRow] = n_geom
                            if n_geom.area < lockdown_size:
                                print('   Location found for cycle {}'.format(iRow))
                                self.data.loc[iRow, 'next_ind'] = iRow
                                self.data.loc[iRow, 'prev_ind'] = iRow
                                self.data.loc[iRow, 'POSITION_QC'] = 3
                                self.data.loc[iRow, 'speed'] = speed
                        else:
                            print('path unable to resolve on cycle {}'.format(row.prev_ind))
                            if stop_on_fail:
                                self.data.geometry = old_geom.copy()
                                return speed
                            self.data.loc[iRow, 'next_ind'] = iRow
                            self.data.loc[iRow, 'prev_ind'] = iRow
                            self.data.loc[iRow, 'POSITION_QC'] = 3
                            self.data.loc[iRow, 'speed'] = speed

            self.data.geometry = curr_geom.copy()

        return speed

    def drop_missing(self):
        ind = np.logical_or.reduce(
            [np.isnan(self.data.max_pressure), np.isnan(self.data.JULD), self.data.max_pressure < 100])
        self.dropped_data = self.data.loc[ind].copy()
        self.data = self.data.loc[~ind].copy()

    def project_geom_to_lat_lon(self):
        df = pd.DataFrame(self.data[[col for col in self.data.columns if col != 'geometry']])

        geom = []

        project = partial(pyproj.transform,
                          self.p2, self.p1)
        print('projecting ...')
        # Reproject back to lat, lon
        for iRow, row in self.data.iterrows():
            if row.POSITION_QC < 4:
                this_geom = row.geometry

                geom.append(transform(project, this_geom))
            else:
                geom.append(None)

        ind = df.POSITION_QC == 3
        df.loc[ind, 'longitude'], df.loc[ind, 'latitude'] = pyproj.transform(self.p2, self.p1, self.data.loc[ind, 'predict_x'].values,
                                                                self.data.loc[ind, 'predict_y'].values)

        self.data = GeoDataFrame(df, crs=self.p1, geometry=geom)

    def get_area_of_interest(self, buffer=0):
        aoi_lat = [self.data.LATITUDE.min() - buffer, self.data.LATITUDE.max() + buffer]
        aoi_lon =  [self.data.LONGITUDE.min() - buffer, self.data.LONGITUDE.max() + buffer]

        return aoi_lat, aoi_lon

    def plot_uncertainty_polygon(self, cycles, ax=None, c=[0,1,0]):
        if ax is None:
            fig, ax = plt.subplots()

        ind = cycles > self.data.index.max()
        if any(ind):
            cycles = cycles[~ind]

        ind = cycles < self.data.index.min()
        if any(ind):
            cycles = cycles[~ind]

        for irow, row in self.data.iterrows():
            if np.any(cycles == irow):
                pyFluid.plot_multipolygon(row.geometry, ax, c)



    def export_uncertainty_polygon_to_shp(self, cycles, outfile):
        out_data = self.get_exportable_data_frame(geom='Polygon')
        out_data.to_file(outfile)

    def export_location_to_shp(self, cycles, outfile):
        out_data = self.get_exportable_data_frame(geom='Point')
        out_data.crs = "EPSG:4326"
        out_data.to_file(outfile)
        #return data


    def get_position(self, method='centroid'):
        self.data = self.data.assign(predict_x=None, predict_y=None)

        # Set values to nan before running inc ase no values
        est_easting = np.nan
        est_northing = np.nan

        # Currently only have the centroid method initialised
        if method == 'centroid':
            print(method)
            for iRow, row in self.data.iterrows():
                if row.POSITION_QC == 3:
                    # Get the estimate position
                    if isinstance(row.geometry, geometry.multipolygon.MultiPolygon):
                        area = 0
                        for polygon in row.geometry:
                            if polygon.area > area:
                                area = polygon.area
                                est_easting = polygon.centroid.x
                                est_northing = polygon.centroid.y
                    else:
                        est_easting = row.geometry.centroid.x
                        est_northing = row.geometry.centroid.y

                self.data.loc[iRow, 'predict_x'] = est_easting
                self.data.loc[iRow, 'predict_y'] = est_northing

    def export_location_to_mat(self,  outfile):
        out_data = self.get_exportable_data_frame(drop_fields='geometry')

        mat_file_out = {name: col.values for name, col in out_data.items()}
        sio.savemat(outfile, mat_file_out)

    def get_exportable_data_frame(self, drop_fields=None, geom='Polygon'):
        """Returns a data frame with the required cycles and fields to export to shape or mat formats
        """

        ind_start = np.logical_and(self.data.index >= self.prediction['start_cycle'],
                                   self.data.index <= self.prediction['end_cycle'])

        data = self.data.loc[ind_start, :].copy()
        position_type = ['', 'GPS', 'Iridium', 'pyFloat Algorithm', '', '', '', '', 'Under Ice Unresolved',
                         'Missing Cycle', '']
        data.loc[np.isnan(data.POSITION_QC), 'POSITION_QC'] = 9

        cols = ['Profile_Number', 'date', 'time', 'latitude', 'longitude', 'POSITION_QC', 'Pos_Type', 'speed', 'max_pressure',
                'min_pressure', 'ice_constrained', 'geometry']
        data.loc[:, 'Profile_Number'] = data.index
        data.loc[:, 'Pos_Type'] = np.array(position_type)[data.POSITION_QC.astype(int).values]
        data.loc[:, 'ice_constrained'] = 0
        data.loc[:, 'ice_constrained'] = 0
        data.loc[:, 'ice_constrained'] = (self.data.apply_iceberg.astype(float) == 1).astype(int)

        try:
            data.loc[:, 'date_time'] = pd.to_datetime(data.JULD, origin='julian',unit='D')
            data.loc[:, 'date'] = data['date_time'].apply(lambda x: x.strftime('%m-%d-%Y'))
            data.loc[:, 'time'] = data['date_time'].apply(lambda x: x.strftime('%H:%M'))
        except:
            data.loc[:, 'date_time'] = ''
            data.loc[:, 'date'] = ''
            data.loc[:, 'time'] = ''
            data.loc[:, 'JULD'] = data.JULD

        if geom == 'Point':
            for iRow, row in data.iterrows():
                p = geometry.Point(row.longitude, row.latitude)
                data.loc[iRow, 'geometry'] = p
        else:
            data.set_index('Profile_Number', drop=True)
            data.crs = "EPSG:4326"

        data = data[cols]

        if drop_fields is not None:
            data = data.drop(columns=drop_fields)

        return data
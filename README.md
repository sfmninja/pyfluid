pyFluid
=======

[https://gitlab.com/sfmninja/pyfluid](https://gitlab.com/sfmninja/pyfluid)

Pyfluid is a python package for the creation of float trajectories from sounding and
bathymetry data. Implementation of an algorithm to estimate the position of ARGO profiling floats that 
spend time under ice during their deployment.

The algorithm is describe in Wallace et al. (https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2020GL087019). Please cite this paper if using the algorithm in your work.

**Requirements**
- [Python](http://www.python.org/) 3.5 or later
- [NumPy](http://docs.scipy.org/doc/numpy/reference/) (base N-dimensional array package)
- [Scipy](http://docs.scipy.org/doc/scipy/reference/) (scientific calculation package)
- [Pandas](https://pandas.pydata.org/) (data analysis and structure library)
- [Geopandas](http://geopandas.org/) (geospatial pandas library)
- [Matplotlib](https://matplotlib.org/) (plotting tools)
- [pyproj](https://pypi.org/project/pyproj/) (python interface to PROJ tools)
- [cartopy](https://scitools.org.uk/cartopy/docs/latest/) (geospatial data processing and mapping)
- [scikit-image](https://scikit-image.org/) (python image processing)
- [xlrd](https://xlrd.readthedocs.io/en/latest/) (excel file reader)
- [cmocean](https://matplotlib.org/cmocean/) (oceanography colormaps)
- [seaborn](https://seaborn.pydata.org/) (data visualisation tools)

**Installation**
```sh
python setup.py install
```

**Contact**

[Luke Wallace](luke.wallace2@rmit.edu.au)

**Example**

```An example script is provided in examples/run_algorithm.py. Example data is avaliable in the data folder of this repository or from https://doi.org/10.6084/m9.figshare.12015126```

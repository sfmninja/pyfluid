import rasterio as rio
import pandas as pd
from scipy.stats import pearsonr

ref = pd.read_csv('C:\\Users\\low\\Downloads\\ref_data.csv')
src = rio.open('C:\\Users\\low\\Downloads\\aci1.asc')

for i, row in ref.iterrows():
    vals = src.sample([(row.Easting, row.Northing)])
    for val in vals:
        ref.loc[i, 'ACI1'] = val

for f in ['ff', 'fc', 'CP']:
    r, p = pearsonr(ref[f], ref.ACI1)
    print(f'{f} ~ ACI1: r = {r:0.2}')
    ref.plot.scatter(f, 'ACI1')
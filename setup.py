import sys
from setuptools import setup

if sys.version_info < (3, 5):
    sys.exit('Python 3.5 is the minimum required version')

INSTALL_REQUIRES = [
    'xarray',
    'numpy',
    'pandas',
    'geopandas',
    'scipy',
    'matplotlib',
    'pyproj',
    'fiona',
    'shapely',
    'cartopy',
    'scikit-image',
    'descartes',
    'cmocean',
    'xlrd',
    'seaborn',
    'xarray',
    'rasterio'
]

setup(
    name='pyFluid',
    version='0.0.10',
    packages=['pyFluid'],
    url='',
    license='',
    author='Luke Wallace',
    author_email='luke.wallace@rmit.edu.au',
    description='An algorithm to estimate the location of ARGO floats while under float',
    py_modules=['pyfloat'],
    install_requires=INSTALL_REQUIRES
)
